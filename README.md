# VUT Brno FIT 2019 - IOS 2. Assignment validator

## Prepare
```sh
cd your/project/folder
git clone https://gitlab.com/anty150/vut-ios-proj2-validator.git
cd vut-ios-proj2-validator
```

## Usage
> For detailed info about any utility execute that utility without arguments.

### Validate existing output
```sh
./validate.sh ../proj2.out P H S R W C [flags]
```

### Run and validate
```sh
./run-and-validate.sh ../proj2.executable P H S R W C [flags]
```

### Execute tests in 'tests' directory
```sh
./run-basic-tests.sh ../proj2.executable
```
> Feel free to define your own tests and share them with others.

## License
Copyright (C) 2019 Jiří Kuchyňka

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.