#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

SCRIPT_DIR=${0%/*}

source "$SCRIPT_DIR/utils/utils.sh"

start_errors

FLAGS="no_i_do_not_want_to_f******_remember_anything_not_even_colors_plus_no_update_check" # :]

OUTPUT_FILE="./proj2.out"
RESULTS_DIR="./tests.out"

DEFAULT_TEST_COUNT=100

TESTS_DIR="$SCRIPT_DIR/tests"
TESTS_OUTPUT_DIR="$SCRIPT_DIR/$RESULTS_DIR"

# ============================== Text...             ==============================

print_usage() {
    printf "Usage: %s ios_proj2_executable_file [N]\n" "$SCRIPT_NAME"
    println
    printf "N - Test repeat count (default %s)\n" "$DEFAULT_TEST_COUNT"
    println
    tail -n +3 "$SCRIPT_DIR/texts/copyright-notice.txt" # Print copyright notice
}

print_remember1() {
    tail -n +3 "$SCRIPT_DIR/texts/remember1.txt" # Print remember1 section
}

print_remember2() {
    tail -n +3 "$SCRIPT_DIR/texts/remember2.txt" # Print remember2 section
}

# ============================== Arguments           ==============================

if [ $# -ne 1 ] && [ $# -ne 2 ]; then
    print_usage
    
    end_errors
    exit 0
fi

executable="$1"
repeat_count="$DEFAULT_TEST_COUNT"
if [ $# -ge 2 ]; then
    repeat_count="$2"
fi

if [ ! -x "$executable" ]; then
    printf "%s: argument ios_proj2_executable_file is not executable\n" "$SCRIPT_NAME" 1>&2
    end_errors
    exit 1
fi

# ============================== Updates             ==============================

CURR_DIR="$( pwd )"
cd "$SCRIPT_DIR" && source "./utils/check-updates.sh"
cd "$CURR_DIR"

# ============================== Actual code         ==============================

RESULT_SUCCESS="success"
RESULT_FAIL="fail"

printf "This script will execute %s times every test in folder 'tests'.\n" "$repeat_count"
cat << EOT
In case of more then 10 fails in sigle test,
  testing by that test will be stopped.
EOT
printf "Failed results will be stored in folder %s.\n" "$RESULTS_DIR"

println
cat << EOT
WARNING: Default test 4 and 5 won't work on merlin server,
    due to process count per user limit on that server.
EOT

println
print_remember1

state_point='-'
update_progress() {
    case "$state_point" in
    -) state_point='\' ;;
    \\) state_point='|' ;;
    \|) state_point='/' ;;
    /) state_point='-' ;;
    esac

    printf "\r[%s] Test: %-15s | %-10s" "$state_point" "$1" "$2"
}

declare -A results

test_file=""
test_name=""
total_fail_count=0
fail_count=0

[ ! -d "$TESTS_OUTPUT_DIR" ] && mkdir "$TESTS_OUTPUT_DIR"
[ -e "$OUTPUT_FILE" ] && rm "$OUTPUT_FILE"

for test_file in "$TESTS_DIR"/*; do
    fail_count=0
    test_name="$( basename "$test_file" )"

    args="$( cat "$test_file" )"

    println
    println "$( tput bold )Executing test $test_name$( tput sgr0 )"

    for i in $( seq 1 $repeat_count ); do
        update_progress "$test_name" "$i"
        if ! log="$( "$SCRIPT_DIR/run-and-validate.sh" "$executable" $args "$FLAGS" 2>&1 )"; then
            [ -e "$OUTPUT_FILE" ] && mv "$OUTPUT_FILE" "$TESTS_OUTPUT_DIR/$test_name-$i-out"
            cat <<< "$log" > "$TESTS_OUTPUT_DIR/$test_name-$i-log"
            fail_count=$(( fail_count + 1 ))
            println
            printf "Test %s-%s failed. Look for log and output file in %s\n" "$test_name" "$i" "$RESULTS_DIR"
        fi
        if [ "$fail_count" -gt 10 ]; then
            println
            println "Reached 10 fails, skipping to another test."
            break
        fi

        [ -e "$OUTPUT_FILE" ] && rm "$OUTPUT_FILE"
    done

    total_fail_count=$(( total_fail_count + fail_count ))

    println
done

println
println "$( tput bold )Testing completed.$( tput sgr0 )"
println "$( tput bold )Found $total_fail_count errors.$( tput sgr0 )"

if [ "$total_fail_count" -eq 0 ]; then
    println
    print_remember2
fi

end_errors
