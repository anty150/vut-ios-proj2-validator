#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

if [ -z "$NO_UPDATE_CHECK" ]; then
    if [ ! -d "./.git" ]; then
        println "Git repository is missing."
        println "Did you clone this repository properly?"
        println "Please delete folder and prepare program again"
        println "  while following steps in README.md"
        end_errors
        exit 1
    fi

    git fetch --all 1>/dev/null 2>&1 || true

    if [ $( git rev-parse HEAD 2>/dev/null ) != $( git rev-parse @{u} 2>/dev/null ) ]; then
        println "Updates available, please run ./update.sh"
        end_errors
        exit 1
    fi
fi
