#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

declare -A known_life_states=(
    ['u']='_unknown_'
    ['r']='_ready_'
    ['s']='_sleep_'
    ['w']='_waits_'
    ['d']='_drives_'
    ['e']='_end of life_'
)

declare -A known_actions=(
    ['starts']="'starts'"
    ['waits']="'waits'"
    ['boards']="'boards'"
    ['leavesqueue']="'leaves queue'"
    ['memberexits']="'member exits'"
    ['captainexits']="'captain exits'"
    ['isback']="'is back'"
)

declare -A known_types=(
    ['HACK']='h'
    ['SERF']='s'
)

declare -a known_boardings_h=(
    [0]=2
    [1]=4
    [2]=0
)

declare -a known_boardings_s=(
    [0]=2
    [1]=0
    [2]=4
)
known_boardings_count=3

declare -A action_target_life_state=(
    ['starts']='r'
    ['waits']='w'
    ['boards']='d'
    ['leavesqueue']='s'
    ['memberexits']='e'
    ['captainexits']='e'
    ['isback']='r'
)

declare -A action_expected_life_state=(
    ['starts']='u'
    ['waits']='r'
    ['boards']='w'
    ['leavesqueue']='r'
    ['memberexits']='w'
    ['captainexits']='d'
    ['isback']='s'
)