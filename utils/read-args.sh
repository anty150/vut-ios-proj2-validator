#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

if [ $# -ne 7 ] && [ $# -ne 6 ]; then
    print_usage
    
    end_errors
    exit 0
fi

argP="$1" # Person count
argH="$2" # Max hack time (in milis)                       !-> this script does not really use this
argS="$3" # Max serf time (in milis)                       !-> this script does not really use this
argR="$4" # Max onboard time                               !-> this script does not really use this
argW="$5" # Max person wait time before return to queue    !-> this script does not really nuse this
argC="$6" # Size of queue

unset NO_UPDATE_CHECK
if grep -q "^.*no..*update_check.*$" <<< "$7"; then
    NO_UPDATE_CHECK="y"
fi

unset NO_SIGNATURE
if grep -q "^.*no..*signature.*$" <<< "$7"; then
    NO_SIGNATURE="y"
fi

unset NO_COLORS
if grep -q "^.*no..*color.*$" <<< "$7"; then
    NO_COLORS="y"
fi

unset NO_REMEMBER
if grep -q "^.*no..*remember.*$" <<< "$7"; then
    NO_REMEMBER="y"
fi

unset NO_F_REMEMBER
if grep -q "^.*no..*f........*remember.*$" <<< "$7"; then
    NO_F_REMEMBER="y"
fi