#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

SCRIPT_NAME="$( basename "$0"; echo n )" && SCRIPT_NAME=${SCRIPT_NAME%??}

CURR_SECTION="$SCRIPT_NAME"

# ============================== Utilities           ==============================

trim() {
    awk '{ $1=$1; print }'
}

rm_spaces() {
    tr -d " \t"
}

exists(){
    if [ "$2" != in ]; then
        printf "Incorrect usage of 'exists' utility.\n"
        printf "Correct usage: exists {key} in {array}\n"
        return
    fi
    eval '[ ${'$3'[$1]+hmm} ]'
}

println() {
    printf "%s\n" "$1"
}

logF() {
    printf "_FATAL_: %s: %s\n" "$CURR_SECTION" "$1"
}

logE() {
    printf "_ERROR_: %s: %s\n" "$CURR_SECTION" "$1"
}

logW() {
    printf "WARNING: %s: %s\n" "$CURR_SECTION" "$1"
}

logN() {
    printf "__NOTE_: %s: %s\n" "$CURR_SECTION" "$1"
}

logT() {
    printf "_______: %s:     %s\n" "$CURR_SECTION" "$1"
}

log() {
    printf "_______: %s: %s\n" "$CURR_SECTION" "$1"
}

err=0
logErr() {
    logE "$1"
    err=$(( err + 1 ))
}

ln_num=0
logErrLn() {
    logErr "Line ${ln_num}: $1"
}

name_id=""
logErrLnName() {
    logErrLn "By '${name_id}': $1"
}

# ============================== Error handling      ==============================

err_abort() {
    printf "%s: %s\\n" "$SCRIPT_NAME" "Execution of script failed..." >&2
    exit 1 # make sure we exit with error code on abort
}

start_errors() {
    trap 'err_abort' 0
    set -e
}

end_errors() {
    trap : 0 # cancel error exit trap -> allow exit with 0
}
