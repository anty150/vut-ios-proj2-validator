#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

declare -a line_types
{ # initialize line_types
    line_types[ 0]='^[1-9][0-9]*:HACK[1-9][0-9]*:starts$'
    line_types[ 1]='^[1-9][0-9]*:HACK[1-9][0-9]*:waits:[0-9][0-9]*:[0-9][0-9]*$'
    line_types[ 2]='^[1-9][0-9]*:HACK[1-9][0-9]*:boards:[0-9][0-9]*:[0-9][0-9]*$'
    line_types[ 3]='^[1-9][0-9]*:HACK[1-9][0-9]*:leavesqueue:[0-9][0-9]*:[0-9][0-9]*$'
    line_types[ 4]='^[1-9][0-9]*:HACK[1-9][0-9]*:memberexits:[0-9][0-9]*:[0-9][0-9]*$'
    line_types[ 5]='^[1-9][0-9]*:HACK[1-9][0-9]*:captainexits:[0-9][0-9]*:[0-9][0-9]*$'
    line_types[ 6]='^[1-9][0-9]*:HACK[1-9][0-9]*:isback$'

    line_types[ 7]='^[1-9][0-9]*:SERF[1-9][0-9]*:starts$'
    line_types[ 8]='^[1-9][0-9]*:SERF[1-9][0-9]*:waits:[0-9][0-9]*:[0-9][0-9]*$'
    line_types[ 9]='^[1-9][0-9]*:SERF[1-9][0-9]*:boards:[0-9][0-9]*:[0-9][0-9]*$'
    line_types[10]='^[1-9][0-9]*:SERF[1-9][0-9]*:leavesqueue:[0-9][0-9]*:[0-9][0-9]*$'
    line_types[11]='^[1-9][0-9]*:SERF[1-9][0-9]*:memberexits:[0-9][0-9]*:[0-9][0-9]*$'
    line_types[12]='^[1-9][0-9]*:SERF[1-9][0-9]*:captainexits:[0-9][0-9]*:[0-9][0-9]*$'
    line_types[13]='^[1-9][0-9]*:SERF[1-9][0-9]*:isback$'
}

LH_START=0
LH_WAITS=1
LH_BOARDS=2
LH_LEAVES=3
LH_MEM_EXIT=4
LH_CAP_EXIT=5
LH_BACK=6

LS_START=7
LS_WAITS=8
LS_BOARDS=9
LS_LEAVES=10
LS_MEM_EXIT=11
LS_CAP_EXIT=12
LS_BACK=13