#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

line_A() {
    cut -s -d ':' -f 1
}

line_NAME_ID() {
    cut -s -d ':' -f 2
}

line_ACTION() {
    cut -s -d ':' -f 3
}

line_NH() {
    cut -s -d ':' -f 4
}

line_NS() {
    cut -s -d ':' -f 5
}

NAME_ID_to_NAME() {
    cut -c 1-4 # take first 4 characters
}

line_NAME() {
    line_NAME_ID | NAME_ID_to_NAME
}

NAME_ID_to_ID() {
    cut -c 5- # take characters 5-(end of line)
}

line_ID() {
    line_NAME_ID | NAME_ID_to_ID
}
