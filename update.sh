#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

SCRIPT_DIR=${0%/*}

set -e

if cd "$SCRIPT_DIR" && git fetch --all && git pull; then
    printf "\n"
    printf "%s\n" "Succesfully updated!"
else
    printf "\n" 1>&2
    printf "%s\n" "Something went wrong. :(" 1>&2
fi