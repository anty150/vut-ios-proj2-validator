#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

SCRIPT_DIR=${0%/*}

source "$SCRIPT_DIR/utils/utils.sh"

start_errors

# ============================== Text...             ==============================

print_err_signature() {
    if [ -z "$NO_SIGNATURE" ]; then
        println
        tail -n +3 "$SCRIPT_DIR/texts/err-signature.txt" # Print error ascii image
    fi
}

print_remember1() {
    if [ -z "$NO_REMEMBER" ]; then
        tail -n +3 "$SCRIPT_DIR/texts/remember1.txt" # Print remember1 section
    else
        if [ -z "$NO_F_REMEMBER" ]; then
            println "Don't forget to REMEMBER!"
        fi
    fi
}

print_remember2() {
    if [ -z "$NO_REMEMBER" ]; then
        tail -n +3 "$SCRIPT_DIR/texts/remember2.txt" # Print remember2 section
    fi
}

print_usage() {
    printf "Usage: %s ios_proj2_output_file P H S R W C [flags]\n" "$SCRIPT_NAME"
    println
    tail -n +3 "$SCRIPT_DIR/texts/args-description.txt" # Print arguments description
    println
    tail -n +3 "$SCRIPT_DIR/texts/copyright-notice.txt" # Print copyright notice
    println
    print_remember1
}

# ============================== Arguments           ==============================

if [ $# -ge 1 ]; then
    OUT_FILE="$1"
    shift
fi
source "$SCRIPT_DIR/utils/read-args.sh"

if [ ! -f "$OUT_FILE" ]; then
    logF "Output file does not exists: '$OUT_FILE'\n" 1>&2
    end_errors
    exit 1
fi

# ============================== Updates             ==============================

CURR_DIR="$( pwd )"
cd "$SCRIPT_DIR" && source "./utils/check-updates.sh"
cd "$CURR_DIR"

# ============================== Actual code         ==============================

source "$SCRIPT_DIR/utils/log-txt-templates.sh"
source "$SCRIPT_DIR/utils/line-types.sh"
source "$SCRIPT_DIR/utils/process-life-description.sh"
source "$SCRIPT_DIR/utils/out-line-utils.sh"

# Ids of checks
CH_SYNTAX="SYNTAX"
CH_LINE_NUMS="LINE_NUMS"
CH_INTEGRITY_P1="INTEGRITY_P1"
CH_QUEUE_FLOW="QUEUE_FLOW"
CH_INTEGRITY_P2="INTEGRITY_P2"

check_syntax() { # checks file syntax
    # This check was inspired by check-syntax.sh script available to download on assigment page.
    
    rm_spaces | {
        sed -f <(
            {
                for i in `seq 0 13`
                do
                    printf "/%s/d\n" "${line_types[$i]}"
                done
            }
            ) | {
            syntax_err=0
            if read -r line; then
                syntax_err=1
                logE "Invalid lines (without spaces):"
                printf "%s\n" "$line"
            fi
            cat
            if [ "$syntax_err" -eq 1 ]; then
                return 1
            fi
        } >&2
    }
}

check_line_nums() { # checks order of line ids - 'A'
    ln_num=0
    
    rm_spaces | line_A | {
        while read -r line; do
            ln_num=$(( ln_num + 1 ))
            
            if [ "$line" -ne "$ln_num" ]; then
                logE "Line ${ln_num}: Found mismach of line id!"
                logT "$( txt_expected_got "${ln_num}" "${line}" )"
                log "Remaining lines will be ignored..."
                return 1 # no reason to continue checking next lines
            fi
        done
    } >&2
}

check_basic_integrity() { # does some basic checks like presence of 'isback' for every 'leavesqueue', etc.
    rm_spaces | {
        err=0
        ln_num=0
        name_id=""
        
        declare -A curr_life_states
        curr_boarded=0
        curr_captain=""
        
        while read -r line; do
            ln_num=$(( ln_num + 1 ))
            
            name_id="$( line_NAME_ID <<< "$line" )"
            action="$( line_ACTION <<< "$line" )"
            action_name="${known_actions["$action"]}"
            
            ls_current='u' # current process life state
            if exists "$name_id" in curr_life_states; then
                ls_current=${curr_life_states["$name_id"]}
            fi
            
            { # update life state
                ls_target="${action_target_life_state["$action"]}"
                ls_expected="${action_expected_life_state["$action"]}"
                
                ls_target_name="${known_life_states["$ls_target"]}"
                ls_expected_name="${known_life_states["$ls_expected"]}"
                
                ls_current_name="${known_life_states["$ls_current"]}"
                
                if [ "$ls_expected" == 'u' ]; then
                    # Start of new process is expected
                    if exists "$name_id" in curr_life_states; then
                        logErrLnName "Received $action_name by process which already reported some activity"
                        logT "$( txt_expected_got "${ls_expected_name}" "${ls_current_name}" )"
                    fi
                else
                    # Process should be already running
                    if ! exists "$name_id" in curr_life_states; then
                        logErrLnName "Received $action_name by process which didn't report any activity yet"
                        logT "$( txt_expected_got "${ls_expected_name}" "${ls_current_name}" )"
                        elif [ "${curr_life_states["$name_id"]}" != "$ls_expected" ]; then
                        logErrLnName "Received $action_name by process which is not in $ls_expected_name state"
                        logT "$( txt_expected_got "${ls_expected_name}" "${ls_current_name}" )"
                    fi
                fi
                
                curr_life_states["$name_id"]="$ls_target"
            }
            
            { # update boarding
                case "$action" in
                    boards)
                        if [ "${curr_boarded}" -ne 0 ]; then
                            logErrLnName "Possible two $action_name without all ${known_actions['memberexits']} and ${known_actions['captainexits']}"
                        fi
                        if [ -n "${curr_captain}" ]; then
                            logErrLnName "Possible two $action_name without ${known_actions['captainexits']}"
                        fi
                        
                        curr_captain="${name_id}"
                        curr_boarded=$(( ${curr_boarded} + 4 ))
                    ;;
                    memberexits)
                        if [ "${curr_boarded}" -le 0 ]; then
                            logErrLnName "Possible $action_name without proper ${known_actions['boards']} or WTF are you doing??"
                        fi
                        if [ "${curr_boarded}" -eq 1 ]; then
                            logErrLnName "Possible $action_name after ${known_actions['captainexits']} or WTF is happening??"
                        fi
                        
                        curr_boarded=$(( ${curr_boarded} - 1 ))
                    ;;
                    captainexits)
                        if [ "${curr_boarded}" -le 0 ]; then
                            logErrLnName "Possible $action_name without proper ${known_actions['boards']} or WTF are you doing??"
                        fi
                        if [ "${curr_boarded}" -gt 1 ]; then
                            logErrLnName "Possible $action_name without all ${known_actions['memberexits']} or WTF is happening??"
                        fi
                        if [ -z "${curr_captain}" ]; then
                            logErrLnName "Possible two $action_name without ${known_actions['boards']}"
                        fi
                        
                        curr_captain=""
                        curr_boarded=$(( ${curr_boarded} - 1 ))
                    ;;
                esac
            }
        done
        
        if [ "$curr_boarded" -ne 0 ]; then
            logErr "Some people was boarded on ship, but never unboarded or vice versa"
            logT "People on ship => $( txt_expected_got "0" "$curr_boarded" )"
        fi
        
        if [ -n "${curr_captain}" ]; then
            logErr "By '${curr_captain}': Possible missing last ${known_actions['captainexits']}"
            logT "captain => $( txt_expected_got "" "$curr_captain" )"
        fi
        
        for name_id in "${!curr_life_states[@]}"; do
            curr_ls="${curr_life_states[$name_id]}"
            if [ "$curr_ls" != 'e' ]; then
                logErr "By '${name_id}': One of precesses didn't end in ${known_life_states['e']} state"
                logT "$( txt_expected_got "${known_life_states['e']}" "${known_life_states["$curr_ls"]}" )"
            fi
        done
        
        if [ "$err" -ne 0 ]; then
            logN "Found $err errors"
            return 1
        fi
    } >&2
}

check_queue_flow() { # checks if queue changes correctly reflects all changes
    rm_spaces | {
        err=0
        ln_num=0
        name_id=""
        
        curr_qh=0
        curr_qs=0
        
        while read -r line; do
            ln_num=$(( ln_num + 1 ))
            
            name_id="$( line_NAME_ID <<< "$line" )"
            name="$( NAME_ID_to_NAME <<< "$name_id" )"
            action="$( line_ACTION <<< "$line" )"
            action_name="${known_actions["$action"]}"
            
            nh="$( line_NH <<< "$line" )"
            ns="$( line_NS <<< "$line" )"
            
            name_type="${known_types["$name"]}"
            
            expected_qh="$curr_qh"
            expected_qs="$curr_qs"
            
            case "$action" in
                waits)
                    eval "expected_q${name_type}=\$(( expected_q${name_type} + 1 ))"
                ;;
                boards)
                    found='n'
                    for i in $( seq 0 $(( $known_boardings_count - 1 )) ); do
                        expected_qh_tmp="$(( expected_qh - ${known_boardings_h[$i]} ))"
                        expected_qs_tmp="$(( expected_qs - ${known_boardings_s[$i]} ))"
                        if [ "$expected_qh_tmp" -eq "$nh" ] && [ "$expected_qs_tmp" -eq "$ns" ]; then
                            found='y'
                            expected_qh="$expected_qh_tmp"
                            expected_qs="$expected_qs_tmp"
                        fi
                    done
                    
                    if [ "$found" != 'y' ]; then
                        logErrLn "Invalid queue change for $action_name"
                        
                        expected_qh_tmp="$(( expected_qh - ${known_boardings_h[0]} ))"
                        expected_qs_tmp="$(( expected_qs - ${known_boardings_s[0]} ))"
                        
                        logT "NH => $( txt_expected_got "$expected_qh_tmp" "$nh" )"
                        logT "NS => $( txt_expected_got "$expected_qs_tmp" "$ns" )"
                        
                        for i in $( seq 0 $(( $known_boardings_count - 1 )) ); do
                            expected_qh_tmp="$(( expected_qh - ${known_boardings_h[$i]} ))"
                            expected_qs_tmp="$(( expected_qs - ${known_boardings_s[$i]} ))"
                            
                            logT "    or"
                            logT "NH => $( txt_expected_got "$expected_qh_tmp" "$nh" )"
                            logT "NS => $( txt_expected_got "$expected_qs_tmp" "$ns" )"
                        done
                        expected_qh="$nh"
                        expected_qs="$ns"
                    fi
                ;;
            esac
            
            if [ -n "$nh" ] && [ -n "$ns" ]; then
                if [ "$nh" -ne "$expected_qh" ]; then
                    logErrLn "Unexpected NH queue jump after $action_name"
                    logT "NH => $( txt_expected_got "$expected_qh" "$nh" )"
                fi
                if [ "$ns" -ne "$expected_qs" ]; then
                    logErrLn "Unexpected NS queue jump after $action_name"
                    logT "NS => $( txt_expected_got "$expected_qs" "$ns" )"
                fi

                curr_qh="$nh"
                curr_qs="$ns"
                
                if [ "$nh" -lt 0 ]; then
                    logErrLn "Negative NH queue after $action_name"
                    logT "$( txt_expected_got ">=0" "$nh" )"
                fi
                if [ "$ns" -lt 0 ]; then
                    logErrLn "Negative NS queue after $action_name"
                    logT "$( txt_expected_got ">=0" "$ns" )"
                fi
            else
                curr_qh="$expected_qh"
                curr_qs="$expected_qs"
            fi
        done
        
        if [ "$curr_qh" -ne 0 ]; then
            logErr "Queue NH mismach: Queue is not empty at the end of output"
            logT "$( txt_expected_got "0" "$curr_qh" )"
        fi
        
        if [ "$curr_qs" -ne 0 ]; then
            logErr "Queue NS mismach: Queue is not empty at the end of output"
            logT "$( txt_expected_got "0" "$curr_qs" )"
        fi
        
        if [ "$err" -ne 0 ]; then
            logN "Found $err errors"
            return 1
        fi
    } >&2
}

check_complete_integrity() { # checks if output correctly represents program arguments
    rm_spaces | {
        err=0
        ln_num=0
        name_id=""
        
        declare -A created
        
        while read -r line; do
            ln_num=$(( ln_num + 1 ))
            
            name_id="$( line_NAME_ID <<< "$line" )"
            name="$( NAME_ID_to_NAME <<< "$name_id" )"
            action="$( line_ACTION <<< "$line" )"
            action_name="${known_actions["$action"]}"
            
            nh="$( line_NH <<< "$line" )"
            ns="$( line_NS <<< "$line" )"
            
            name_type="${known_types["$name"]}"
            
            case "$action" in
                starts)
                    created["$name_id"]='y'
                ;;
                waits)
                    if [ "$(( nh + ns ))" -gt "$argC" ]; then
                        logErrLnName "Queue overflow in $action_name"
                        logT "Queue length => $( txt_expected_got "0-$argC" "$nh + $ns" )"
                    fi
                ;;
                leavesqueue)
                    if [ "$(( nh + ns ))" -lt "$argC" ]; then
                        logErrLnName "Detected $action_name but queue is not full"
                        logT "Queue length => $( txt_expected_got "$argC" "$nh + $ns" )"
                    fi
                ;;
            esac
        done
        
        check_process() {
            if ! exists "$name_id" in created; then
                logErr "Missing process ${name_id}"
            fi
        }
        for i in $( seq 1 $argP ); do
            name_id="HACK${i}"
            check_process

            name_id="SERF${i}"
            check_process
        done

        for name_id in "${!created[@]}"; do
            id=$( NAME_ID_to_ID <<< "$name_id" )
            if [ "$id" -lt 1 ] || [ "$id" -gt "$argP" ]; then
                logErr "Process ${name_id} doesn't have id within range of P argument"
                logT "$( txt_expected_got "1-$argP" "$id" )"
            fi
        done
        
        if [ "$err" -ne 0 ]; then
            logN "Found $err errors"
            return 1
        fi
    } >&2
}

exit_check_fail() {
    print_err_signature >&2
    println 1>&2
    println "The remaining tests will be skipped due to previous errors." >&2
    
    end_errors
    exit 1 # some check failed - can't run remaining tests...
}

run_check() {
    CURR_SECTION="$2"
    
    println
    if [ -z "$NO_COLORS" ]; then
        printf "$( tput bold )%-80s - %-15s$( tput sgr0 )\n" "$3" "$2"
    else
        printf "%-80s - %-15s\n" "$3" "$2"
    fi
    eval "if ! $1 < \"$OUT_FILE\"; then exit_check_fail; fi"
    
    CURR_SECTION="$SCRIPT_NAME"
}

print_remember1

run_check check_syntax             "$CH_SYNTAX"       "Checking lines syntax"
run_check check_line_nums          "$CH_LINE_NUMS"    "Checking lines numbers ('A')"
run_check check_basic_integrity    "$CH_INTEGRITY_P1" "Checking basic integrity (leave-back + board-exit pairs, processes life cycle)"
run_check check_queue_flow         "$CH_QUEUE_FLOW"   "Checking queue flow ('NH' and 'NS')"
run_check check_complete_integrity "$CH_INTEGRITY_P2" "Checking complete integrity (synchronization behaviour + arguments compliance)"

println
println "Validation succesfully completed!"

print_remember2

end_errors
