#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

SCRIPT_DIR=${0%/*}

source "$SCRIPT_DIR/utils/utils.sh"

start_errors

OUTPUT_FILE="./proj2.out"

# ============================== Text...             ==============================

print_usage() {
    printf "Usage: %s ios_proj2_executable_file P H S R W C [flags]\n" "$SCRIPT_NAME"
    println
    tail -n +3 "$SCRIPT_DIR/texts/args-description.txt" # Print arguments description
    println
    tail -n +3 "$SCRIPT_DIR/texts/copyright-notice.txt" # Print copyright notice
}

# ============================== Arguments           ==============================

if [ $# -ge 1 ]; then
    executable="$1"
    shift
else
    print_usage
    
    end_errors
    exit 0
fi

if [ ! -x "$executable" ]; then
    printf "%s: argument ios_proj2_executable_file is not executable\n" "$SCRIPT_NAME" 1>&2
    end_errors
    exit 1
fi

# ============================== Actual code         ==============================

[ -f "$OUTPUT_FILE" ] && {
    printf "Removing existing output file: '%s'\n" "$OUTPUT_FILE"
    println
    rm "$OUTPUT_FILE"
}

printf "Executing ios_proj2_executable_file: '%s'\n" "$executable ${*:1:6}"

if ! "$executable" "${@:1:6}"; then
    printf "%s: failed to execute ios_proj2_executable_file: '%s'\n" "$SCRIPT_NAME" "$executable" 1>&2
    end_errors
    exit 1
fi

if [ ! -f "$OUTPUT_FILE" ]; then
    printf "Project output file '%s' is missing?! Nothing to validate...\n" "$OUTPUT_FILE" 1>&2
    end_errors
    exit 1
fi

println
printf "Executing validation script: '%s'\n" "$SCRIPT_DIR/validate.sh $OUTPUT_FILE $*"

exec "$SCRIPT_DIR/validate.sh" "$OUTPUT_FILE" "$@"

# end_errors
