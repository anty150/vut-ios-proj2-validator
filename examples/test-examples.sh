#!/bin/bash

# Copyright (C) 2019 Jiří Kuchyňka
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see http://www.gnu.org/licenses/.

SCRIPT_DIR=${0%/*}

source "$SCRIPT_DIR/../utils/utils.sh"

start_errors

FLAGS="no_f******_signature_and_no_i_do_not_want_to_remember_anything_plus_no_update_check" # :]

FAIL_EXAMPLES_DIR="$SCRIPT_DIR/fail-examples"
CORRECT_EXAMPLES_DIR="$SCRIPT_DIR/correct-examples"

# ============================== Text...             ==============================

print_usage() {
    printf "Usage: %s\n" "$SCRIPT_NAME"
}

# ============================== Arguments           ==============================

if [ $# -ne 0 ]; then
    print_usage
    
    end_errors
    exit 0
fi

# ============================== Updates             ==============================

CURR_DIR="$( pwd )"
cd "$SCRIPT_DIR/.." && source "./utils/check-updates.sh"
cd "$CURR_DIR"

# ============================== Actual code         ==============================

source "$SCRIPT_DIR/../utils/log-txt-templates.sh"

RESULT_SUCCESS="success"
RESULT_FAIL="fail"
RESULT_SKIP="skip"
        
declare -A fail_results
declare -A correct_results

example_dir=""
example_name=""
run_example() {
    args_file="$example_dir/args"
    output_file="$example_dir/out"
    
    println
    if [ -e "$args_file" ] && [ -e "$output_file" ]; then
        printf -- "$( tput bold )-- Executing example %s:$( tput sgr0 )\n" "$example_name"

        args="$( cat "$args_file" )"
        if "$SCRIPT_DIR/../validate.sh" "$output_file" $args "$FLAGS"; then
            printf "%s" "$RESULT_SUCCESS" 1>&3
        else
            printf "%s" "$RESULT_FAIL" 1>&3
        fi
    else
        printf -- "$( tput bold )-- Skipping example %s$( tput sgr0 )\n" "$example_name"
        printf "%s" "$RESULT_SKIP" 1>&3
    fi
}

printf -- "$( tput bold )- RUNNING %s$( tput sgr0 )\n" "$( basename "$FAIL_EXAMPLES_DIR" )"

for example_dir in "$FAIL_EXAMPLES_DIR"/*; do
    example_name="$( basename "$example_dir" )"
    fail_results["$example_name"]="$( run_example 3>&1 1>&2 )"
done

println
println
printf -- "$( tput bold )- RUNNING %s$( tput sgr0 )\n" "$( basename "$CORRECT_EXAMPLES_DIR" )"

for example_dir in "$CORRECT_EXAMPLES_DIR"/*; do
    example_name="$( basename "$example_dir" )"
    correct_results["$example_name"]="$( run_example 3>&1 1>&2 )"
done

println
println
println
println "$( tput bold )- Results:$( tput sgr0 )"

err=0

process_result() {
    expected_result="$1"
    example_name="$2"
    example_result="$3"
    printf -- "-- %-30s => %s\n" "$example_name" "$( txt_expected_got "$expected_result" "$example_result" )"
    if [ "$example_result" != "$expected_result" ]; then
        err=$(( err + 1 ))
    fi
}

for example_name in "${!fail_results[@]}"; do
    process_result "$RESULT_FAIL" "$example_name" "${fail_results["$example_name"]}"
done

for example_name in "${!correct_results[@]}"; do
    process_result "$RESULT_SUCCESS" "$example_name" "${correct_results["$example_name"]}"
done

println
printf "$( tput bold )Found %s errors$( tput sgr0 )\n" "$err"

end_errors
